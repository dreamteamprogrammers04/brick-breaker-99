extends RigidBody2D


const SPEEDUP = 400
const MAXSPEED = 3000

func _ready():
	set_physics_process(true)
	
func _physics_process(delta):
	var bodies = get_colliding_bodies()
	
	for body in bodies:
		if body.get_parent().get_name() == "RedBricks":
			body.queue_free()
	for body in bodies:
		if body.get_parent().get_name() == "BlueBricks":
			body.queue_free()
			
	for body in bodies:
		if body.get_parent().get_name() == "GreenBricks":
			body.queue_free()
			
		if body.get_name() == "Paddle":
			var speed = get_linear_velocity().length()
			var direction = get_position() - body.get_node("Ancor").get_global_position()
			var velocity = direction.normalized()*min(100 + SPEEDUP, MAXSPEED)
			set_linear_velocity(velocity) 
	if get_position().y > get_viewport_rect().end.y:
		print("Ball died.")
		queue_free()
			
	

