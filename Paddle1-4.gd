extends KinematicBody2D

var motion = Vector2()
const ball_scene = preload("res://Ball.tscn") 

func _process(delta):
	if Input.is_action_pressed('ui_right'):
		motion.x = 400
		move_and_slide(motion)
	elif Input.is_action_pressed('ui_left'):
		motion.x = -400
		move_and_slide(motion)
	
